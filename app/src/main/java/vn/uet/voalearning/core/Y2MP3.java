package vn.uet.voalearning.core;

import vn.uet.voalearning.utils.HttpClientService;

/**
 * Using YoutubeinMp3's API
 * Created by cuong on 3/31/2016.
 */
public class Y2MP3 {
    private static final String REQUEST_URL_PATTERN =
            "http://www.youtubeinmp3.com/fetch/?format=JSON&video=%s";
    private static final String CONVERT_URL_PATTERN =
            "http://www.youtubeinmp3.com/download/?video=%s&autostart=1";

    public String getMP3(String videoUrl) throws Exception {
        String result =  HttpClientService.getInstance()
                .sendGet(String.format(REQUEST_URL_PATTERN, videoUrl));

        if (result.contains("http-equiv=\"refresh\"")) {
            HttpClientService.getInstance()
                    .sendGet(String.format(CONVERT_URL_PATTERN, videoUrl));

            System.out.println("\nWating to convert...");
            Thread.sleep(10000); // wait 10s for converting

            result = HttpClientService.getInstance()
                    .sendGet(String.format(REQUEST_URL_PATTERN, videoUrl));
        }

        return result;
    }
}
