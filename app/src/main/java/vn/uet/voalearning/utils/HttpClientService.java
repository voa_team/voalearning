package vn.uet.voalearning.utils;

import java.net.URL;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/**
 * Created by cuong on 3/31/2016.
 */
public class HttpClientService {
    private static HttpClientService ourInstance = new HttpClientService();
    private HttpURLConnection con;

    private HttpClientService() {}
    public static HttpClientService getInstance() {
        return ourInstance;
    }

    public String sendGet(String url) throws Exception {

        URL obj = new URL(url);
        con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setConnectTimeout(3000); // set timeout 3s

//        int responseCode = con.getResponseCode();
//        System.out.println("\nSending 'GET' request to URL : " + url);
//        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }
}
